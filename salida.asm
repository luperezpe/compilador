

segment .data

	_div0_msg db "****Error de ejecucion: Division por cero.",0
	_ind_out_lims_msg db "****Error de ejecucion: Indice fuera de rango.",0

segment .bss

	_v1 resd 1
	_v2 resd 1

segment .text
global main
extern scan_int, scan_boolean
extern print_int,  print_boolean, print_string, print_blank, print_endofline

main:
	push dword _v1
	call scan_int
	add esp,4
	push dword _v2
	call scan_int
	add esp,4
	push dword _v1
	push dword _v2
	pop dword edx
	mov dword edx, [edx]
	pop dword eax
	mov dword eax, [eax]
	add eax, edx
	push dword eax
	call print_int
	add esp, 4
	call print_endofline
	push dword _v1
	push dword _v2
	pop dword edx
	mov dword edx, [edx]
	pop dword eax
	mov dword eax, [eax]
	sub eax, edx
	push dword eax
	call print_int
	add esp, 4
	call print_endofline
	push dword _v1
	push dword _v2
	pop dword edx
	mov dword edx, [edx]
	pop dword eax
	mov dword eax, [eax]
	imul eax, edx
	push dword eax
	call print_int
	add esp, 4
	call print_endofline
	push dword _v1
	push dword _v2
	pop dword ecx
	mov dword ecx, [ecx]
	pop dword eax
	mov dword eax, [eax]
	cmp ecx,0
	je div_cero
	cdq
	idiv ecx
	push dword eax
	call print_int
	add esp, 4
	call print_endofline
	push dword _v1
	pop dword eax
	mov dword eax,[eax]
	neg eax
	push dword eax
	call print_int
	add esp, 4
	call print_endofline
	ret

div_cero:
	push dword _div0_msg
	call print_string
	add esp, 4
	call print_endofline
	ret

ind_out_lims:
	push dword _ind_out_lims_msg
	call print_string
	add esp, 4
	call print_endofline
	ret
