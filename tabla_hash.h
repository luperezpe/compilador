#ifndef _ELEMENTO_TABLA_H
#define _ELEMETO_TABLA_H
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VARIABLE 1
#define PARAMETRO 2
#define FUNCION 3

#define MAX_STRING 50
#define MAX_TAM    500	

typedef struct elemento{
	char nombre[MAX_STRING];/*el tamaño lo podiamos poner como el maximo de un elemento en un define en el *.h*/
	int identificador;
	int categoria;
	int clase;
	int tipo;
	int tamanio;
	int numVarLocal;
	int posVarLocal;
	int numParams;
	int posParam;

	struct elemento *next;

}elemento_tabla;

typedef struct{
		elemento_tabla* tabla;
		int tam;
}tabla_hash;

int crear_tabla (tabla_hash *tabla,int tamanio);
int insertar(elemento_tabla *e,tabla_hash *tabla);
int buscar(elemento_tabla *e, tabla_hash *tabla);
int borra_tabla(tabla_hash *tabla);
int calcula_id(char* cadena);
int buscar_lexema(char* lexema, tabla_hash *tabla);

#endif
