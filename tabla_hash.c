
#include "tabla_hash.h"
/**
*@name int crear_tabla (tabla_hash *tabla,int tamanio)
*@brief La funcion crea una tabla hash.
*@params tamanio: tamanno de la tabla.
*@params tabla: tabla hash que vamos a crear.
*@return 0 si se crea correctamente y -1 si hay algun error.
*/
int crear_tabla (tabla_hash *tabla,int tamanio){
	int i=0;
     //   tabla=malloc(sizeof(tabla_hash));
	tabla->tabla=malloc(tamanio*sizeof(elemento_tabla));
	tabla->tam=tamanio;
	for (i=0;i<tamanio;i++){
		tabla->tabla[i].next=NULL;
		tabla->tabla[i].identificador=-1;
		tabla->tabla[i].categoria=0;
		tabla->tabla[i].clase=0;
		tabla->tabla[i].tipo=0;
		tabla->tabla[i].tamanio=0;
		tabla->tabla[i].numVarLocal=0;
		tabla->tabla[i].posVarLocal=0;
		tabla->tabla[i].numParams=0;
		tabla->tabla[i].posParam=0;
	}
	if(tabla->tabla==NULL){
		return -1;
	}
	return 0;
}
/**
*@name int insertar(elemento_tabla *e,tabla_hash *tabla)
*@brief Función que annade un elemento en la tabla hash.
*@params e: elemento que se quiere insertar.
*@params tabla: tabla hash en donde vamos a introducir el elemento.
*@return 0 si se inserta correctamente y -1 si hay algun error.
*/


int insertar(elemento_tabla *e,tabla_hash *tabla){
	int key;
	elemento_tabla *aux=NULL;
	key=e->identificador%(tabla->tam);
	if(tabla->tabla[key].identificador==-1){
		strcpy(tabla->tabla[key].nombre,e->nombre);
			tabla->tabla[key].identificador=e->identificador;
			tabla->tabla[key].categoria=e->categoria;
			tabla->tabla[key].clase=e->clase;
			tabla->tabla[key].tipo=e->tipo;
			tabla->tabla[key].tamanio=e->tamanio;
			tabla->tabla[key].numVarLocal=e->numVarLocal;
			tabla->tabla[key].posVarLocal=e->posVarLocal;
			tabla->tabla[key].numParams=e->numParams;
			tabla->tabla[key].posParam=e->posParam;
			
	    	tabla->tabla[key].next=NULL;

		return 0;
	}
	else{

		if(0==strcmp(tabla->tabla[key].nombre,e->nombre)){
			return -1;/*el elemento ya esta en la tabla*/
		}/*nuevo,comparo con el nombre*/
	
		if(tabla->tabla[key].next==NULL){
			tabla->tabla[key].next = e;
			
		}
		else{

			for(aux=tabla->tabla[key].next;aux->next!=NULL;aux=aux->next){
				if(0==strcmp(aux->nombre,e->nombre)){
							return -1;/*el elemento ya esta en la tabla*/
				}
			}
		aux->next=e;
		}
	}

	return 0;
}
/**
*@name int buscar(elemento_tabla *e,tabla_hash *tabla)
*@brief Esta funcion busca el elemento deseado en la tabla hash.
*@params e: elemento que se quiere buscar.
*@params tabla: tabla hash en donde vamos a buscar el elemento.
*@return 0 si se inserta si se encuentra y -1 si no.
*/

int buscar(elemento_tabla *e,tabla_hash *tabla){

	elemento_tabla *aux=NULL;
	int key = e->identificador%(tabla->tam);
	if(tabla->tabla[key].identificador!=-1){
		
		if(0==strcmp(tabla->tabla[key].nombre,e->nombre)){
				e->identificador	= tabla->tabla[key].identificador;
				e->next				= tabla->tabla[key].next;
				e->categoria		= tabla->tabla[key].categoria;
				e->clase			= tabla->tabla[key].clase;
				e->tipo				= tabla->tabla[key].tipo;
				e->tamanio			= tabla->tabla[key].tamanio;
				e->numVarLocal		= tabla->tabla[key].numVarLocal;
				e->posVarLocal		= tabla->tabla[key].posVarLocal;
				e->numParams		= tabla->tabla[key].numParams;
				e->posParam			= tabla->tabla[key].posParam;
				return 0;/*esta el elemento*/
		}
		else{ 

			if(tabla->tabla[key].next!=NULL){
				for(aux=tabla->tabla[key].next;aux->next!=NULL;aux=aux->next){
					if(0==strcmp(aux->nombre,e->nombre)){
						e->identificador	= aux->identificador;
						e->next				= aux->next;
						e->categoria		= tabla->tabla[key].categoria;
						e->clase			= tabla->tabla[key].clase;
						e->tipo				= tabla->tabla[key].tipo;
						e->tamanio			= tabla->tabla[key].tamanio;
						e->numVarLocal		= tabla->tabla[key].numVarLocal;
						e->posVarLocal		= tabla->tabla[key].posVarLocal;
						e->numParams		= tabla->tabla[key].numParams;
						e->posParam			= tabla->tabla[key].posParam;
						return 0;
					}
				}
			}else{
				return -1;
			}
			
		}
	}/*si se sale de aqui, no estara en hash*/
	return -1;
}
/**
*@name int borra_tabla(tabla_hash *tabla)
*@brief Borra la tabla hash.
*@params tabla: tabla hash en donde vamos a introducir el elemento.
*@return 0 si se inserta correctamente y -1 si hay algun error.
*/
int borra_tabla(tabla_hash *tabla){
tabla->tam=0;/*faltaria un free*/

return 0;
}

int calcula_id(char* cadena){
	char *aux;
	int identificador=0;

	aux=cadena;
	
	while(*aux!='\0'){
		identificador +=	(int)(*aux);
		aux++;
	}

	return identificador;
}
/**
*@name int buscar_lexema(elemento_tabla *e,tabla_hash *tabla)
*@brief Esta funcion busca el elemento deseado en la tabla hash.
*@params e: elemento que se quiere buscar.
*@params tabla: tabla hash en donde vamos a buscar el elemento.
*@return 0 si se inserta si se encuentra y -1 si no.
*/

int buscar_lexema(char *lexema,tabla_hash *tabla){
	
	elemento_tabla *aux=NULL;
	int key = ( calcula_id(lexema) % (tabla->tam) );
	if(tabla->tabla[key].identificador!=-1){
		
		if(0==strcmp(tabla->tabla[key].nombre,lexema)){
				return 0;/*esta el elemento*/
		}
		else{ 
			if(tabla->tabla[key].next!=NULL){
				for(aux=tabla->tabla[key].next;aux->next!=NULL;aux=aux->next){
					if(0==strcmp(aux->nombre,lexema)){
						return 0;
					}
				}
			}else{
				return -1;
			}
			
		}
	}/*si se sale de aqui, no estara en hash*/
	return -1;
}