#ifndef _GENERADORCODIGO_H
#define _GENERADORCODIGO_H
#include "tabla_hash.h"
#include "alfa.h"

#include <stdio.h>



void escribeCabeceras (FILE *f);
void escribeFin (FILE *f);
void gc_suma_enteros(FILE* f, int es_direccion_op1, int es_direccion_op2, char *lexema1,char *lexema2);
void gc_resta_enteros(FILE* f, int es_direccion_op1, int es_direccion_op2,char *lexema1,char *lexema2);
void gc_not(FILE* f, int es_direccion);
void gc_and(FILE* f, int es_direccion_op1, int es_direccion_op2);
void gc_or(FILE* f, int es_direccion_op1, int es_direccion_op2);
void gc_neg_logico(FILE* f, int es_direccion, int etiqueta);
void gc_comparacion_igual(FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta);
void gc_comparacion_distinto (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta);
void gc_comparacion_menorIgual (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta);
void gc_comparacion_mayorIgual (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta);
void gc_comparacion_menor (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta);
void gc_comparacion_mayor (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta);
void gc_imprime(FILE* f, int es_direccion, int tipo);
void escribirTbSimbolo (elemento_tabla *e, tabla_hash *tabla);
void gc_inicia_elemento_vector(FILE *f, int es_direccion, int tam_vector_actual, char *variable);
void gc_llamadas_fuciones (FILE* f,int valor, char * funcion);
void gc_funcion (FILE* f, int num_var_local, char * nombre_f);
void fin_funcion (FILE* f, int es_direccion);
void gc_condicional(FILE* f,int etiqueta, int es_direccion);
void gc_condSentencia(FILE* f, int etiqueta);
void gc_bucleExp(FILE* f,int es_direccion, int etiqueta);
void gc_inicioBucle(FILE* f, int etiqueta);
void gc_else(FILE* f, int etiqueta);
void gc_mul_enteros(FILE* f, int es_direccion_op1, int es_direccion_op2,char *lexema1,char *lexema2);
void gc_div_enteros(FILE* f, int dividendo, int divisor,char *lexema1,char *lexema2);
void gc_condicional_simple_fin(FILE* f, int etiqueta); 

#endif
