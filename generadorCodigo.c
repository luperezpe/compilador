#include "generadorCodigo.h"

void escribirTbSimbolo (elemento_tabla *e, tabla_hash *tabla){
}

void escribeFin(FILE *f){
    fprintf(f, "jmp near fin\n");                         
    fprintf(f, "error_1:  push dword mensaje_1\n");
    fprintf(f, "call print_string\n");
    fprintf(f, "add esp, 4\n");
    fprintf(f, "call print_endofline\n");
    fprintf(f, "jmp near fin\n");
           
    fprintf(f, "error_2:  push dword mensaje_2\n");
    fprintf(f, "call print_string\n");
    fprintf(f, "add esp, 4\n");
    fprintf(f, "call print_endofline\n");
    fprintf(f, "jmp near fin\n");
        
    fprintf(f, "fin: ret\n");
}

void escribeCabeceras (FILE *f){
    fprintf(f,"segment .data\n");
    fprintf(f,"mensaje_1 db \"ERROR DE EJECUCION: Division por cero\", 0\n");
    fprintf(f,"mensaje_2 db \"ERROR DE EJECUCION: Indice fuera de rango\", 0\n");
    fprintf(f,"segment .text\n");
    fprintf(f,"global main\n");
    fprintf(f,"extern print_int, print_boolean, print_string, print_black, print_endofline\n");
    fprintf(f,"extern scan_int, scan_boolean\n");
    fprintf(f,"main:\n");
}


void gc_suma_enteros(FILE* f, int es_direccion_op1, int es_direccion_op2, char *lexema1,char *lexema2){
    fprintf(f,"push dword _%s\n", lexema1);
    fprintf(f,"push dword _%s\n", lexema2);
    fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; realizar la suma y dejar el resultado en eax\n");
    fprintf(f,"add eax , edx\n");
    fprintf(f,"; apilar el resultado\n");
    fprintf(f,"push dword eax\n");
    return;
}

void gc_resta_enteros(FILE* f, int es_direccion_op1, int es_direccion_op2,char *lexema1,char *lexema2){
    fprintf(f,"push dword _%s\n", lexema1);
    fprintf(f,"push dword _%s\n", lexema2);
    fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; realizar la resta y dejar el resultado en eax\n");
    fprintf(f,"sub eax , edx\n");
    fprintf(f,"; apilar el resultado\n");
    fprintf(f,"push dword eax\n");
    return;
}
void gc_mul_enteros(FILE* f, int es_direccion_op1, int es_direccion_op2,char *lexema1,char *lexema2){
    fprintf(f,"push dword _%s\n", lexema1);
    fprintf(f,"push dword _%s\n", lexema2);
    fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword ecx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword ecx, [ecx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; realizar la resta y dejar el resultado en eax\n");
    fprintf(f,"imul ecx\n");
    fprintf(f,"; apilar el resultado\n");
    fprintf(f,"push dword eax\n");
    return;
}
void gc_div_enteros(FILE* f, int dividendo, int divisor,char *lexema1,char *lexema2){
    fprintf(f,"push dword _%s\n", lexema1);
    fprintf(f,"push dword _%s\n", lexema2);
    fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword ecx\n");
    if(divisor == 1){
        fprintf(f,"mov dword ecx, [ecx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(dividendo == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f, "cmp ecx,0\n" );
    fprintf(f, "je div_cero\n");
    fprintf(f,"; realizar la division y dejar el resultado en eax\n");
    fprintf(f,"; extender el dividendo a la composicion de registros \n");
    fprintf(f, "; cdq extiende el registro eax en edx:eax \n");
    fprintf(f,"cdq\n");
    fprintf(f,"idiv ecx \n");
    fprintf(f,"; apilar el resultado\n");
    fprintf(f,"push dword eax\n");
    return;
}

void gc_not(FILE* f, int es_direccion){
    fprintf(f,"pop dword edx\n");
    if(es_direccion==1){
        fprintf(f,"mov dword eax, [eax]\n");
    }
    fprintf(f,"; realizar la negacion. El resultado en eax\n");
    fprintf(f,"neg eax \n");
    fprintf(f,"; apilar el resultado\n");
    fprintf(f,"push dword eax \n");
}


void gc_and(FILE* f, int es_direccion_op1, int es_direccion_op2){
    fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; realizar la and y dejar el resultado en eax\n");
    fprintf(f,"and eax , edx\n");
    fprintf(f,"; apilar el resultado\n");
    fprintf(f,"push dword eax\n");
    return;
}

void gc_or(FILE* f, int es_direccion_op1, int es_direccion_op2){
    fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; realizar la or y dejar el resultado en eax\n");
    fprintf(f,"or eax , edx\n");
    fprintf(f,"; apilar el resultado\n");
    fprintf(f,"push dword eax\n");
    return;
}

void gc_neg_logico(FILE* f, int es_direccion, int etiqueta){
    fprintf(f,"pop dword edx\n");
    if(es_direccion==1){
        fprintf(f,"mov dword eax, [eax]\n");
    }
    fprintf(f,"; ver si eax es 0 y en ese caso saltar a negar_falso\n");
    
    fprintf(f,"or eax, eax \n");
    fprintf(f,"jz near negar_falso%d \n", etiqueta);
    fprintf(f,"; cargar 0 en eax (negacion de verdadero) y saltar al final\n");
    fprintf(f,"mov dword eax, 0 \n");
    fprintf(f,"jmp near fin_negacion%d \n", etiqueta);
    fprintf(f,"; cargar 1 en eax (negacion de falso) \n");
    fprintf(f,"negar_falso%d: mov dword eax, 1\n", etiqueta);
    fprintf(f,"; apilar eax\n");
    fprintf(f,"fin_negacion%d: push dword eax \n", etiqueta);
}

void gc_comparacion_igual(FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta){
fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; compara y apila el resultado\n");
    fprintf(f,"cmp eax , edx\n");
    fprintf(f,"je near igual%d\n", etiqueta);
    fprintf(f,"push dword 0\n");
    fprintf(f,"jmp near fin_igual%d\n", etiqueta);
    fprintf(f,"igual%d: push dword 1\n", etiqueta);
    fprintf(f,"fin_igual%d:\n", etiqueta);
    return;
}

void gc_comparacion_distinto (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta){
fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; compara y apila el resultado\n");
    fprintf(f,"cmp eax , edx\n");
    fprintf(f,"jne near distinto%d\n", etiqueta);
    fprintf(f,"push dword 0\n");
    fprintf(f,"jmp near fin_distinto%d\n", etiqueta);
    fprintf(f,"distinto%d: push dword 1\n", etiqueta);
    fprintf(f,"fin_distinto%d:\n", etiqueta);
    return;
}


void gc_comparacion_menorIgual (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta){
fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; compara y apila el resultado\n");
    fprintf(f,"cmp eax , edx\n");
    fprintf(f,"jle near menorigual%d\n", etiqueta);
    fprintf(f,"push dword 0\n");
    fprintf(f,"jmp near fin_menorigual%d\n", etiqueta);
    fprintf(f,"menorigual%d: push dword 1\n", etiqueta);
    fprintf(f,"fin_menorigual%d:\n", etiqueta);
    return;
}

void gc_comparacion_mayorIgual (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta){
fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; compara y apila el resultado\n");
    fprintf(f,"cmp eax , edx\n");
    fprintf(f,"jge near mayorigual%d\n",etiqueta);
    fprintf(f,"push dword 0\n");
    fprintf(f,"jmp near fin_mayorigual%d\n",etiqueta);
    fprintf(f,"mayorigual%d: push dword 1\n",etiqueta);
    fprintf(f,"fin_mayorigual%d:\n",etiqueta);
    return;
}

void gc_comparacion_menor (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta){
fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; compara y apila el resultado\n");
    fprintf(f,"cmp eax , edx\n");
    fprintf(f,"jl near menor%d\n",etiqueta);
    fprintf(f,"push dword 0\n");
    fprintf(f,"jmp near fin_menor%d\n",etiqueta);
    fprintf(f,"menor%d: push dword 1\n",etiqueta);
    fprintf(f,"fin_menor%d:\n",etiqueta);
    return;
}

void gc_comparacion_mayor (FILE* f, int es_direccion_op1, int es_direccion_op2, int etiqueta){
    fprintf(f,"; cargar el segundo operando en edx\n");
    fprintf(f,"pop dword edx\n");
    if(es_direccion_op2 == 1){
        fprintf(f,"mov dword edx, [edx]\n");
    }
    fprintf(f,"; cargar el primer operando en eax\n");
    fprintf(f,"pop dword eax\n");
    if(es_direccion_op1 == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    fprintf(f,"; compara y apila el resultado\n");
    fprintf(f,"cmp eax , edx\n");
    fprintf(f,"jg near mayor%d\n", etiqueta);
    fprintf(f,"push dword 0\n");
    fprintf(f,"jmp near fin_mayor%d\n", etiqueta);
    fprintf(f,"mayor%d: push dword 1\n", etiqueta);
    fprintf(f,"fin_mayor%d:\n", etiqueta);
    return;
}
void gc_inicia_elemento_vector(FILE *f, int es_direccion, int tam_vector_actual, char *variable){
    fprintf(f,";Carga del valor del índice en eax\n");
    fprintf(f,"pop dword eax\n");
    if (es_direccion == 1){
        fprintf(f,"mov dword eax , [eax]\n");
    }
    
    fprintf(f,"; Si el índice es menor que 0, error en tiempo de ejecución\n");
    fprintf(f,"cmp eax,0\n");
    fprintf(f,"jl near mensaje_1\n");
    fprintf(f,"; Si el índice es mayor de lo permitido , error en tiempo de ejecución\n");
    fprintf(f,"cmp eax, %d\n", tam_vector_actual-1);
    fprintf(f,"jl near mensaje_1\n");
    fprintf(f,"; Cargar en edx la dirección de inicio del vector\n");
    fprintf(f,"mov dword edx, _%s \n", variable);
    fprintf(f,";Cargar en eax la dirección del elemento indexado\n ");
    fprintf(f,"lea eax, [edx + eax*4]\n");
    fprintf(f,"; Apilar la dirección del elemento indexado\n");
    fprintf(f,"push dword eax\n");

}
void gc_imprime (FILE* f, int es_direccion, int tipo){
    if (es_direccion == 1){
        fprintf(f,"pop dword eax\n");
        fprintf(f,"mov dword eax , [eax]\n");
        fprintf(f,"push dword eax\n");
    }
    switch(tipo){
        case INT:
            fprintf(f,"; expresión es de tipo entero\n");
            fprintf(f,"call print_int\n");
            break;
        case BOOLEAN:
            fprintf(f,"; Si la expresión es de tipo boolean\n");
            fprintf(f,"call print_boolean\n");
            break;
    }
    fprintf(f,"; Restauración del puntero de pila\n");
    fprintf(f,"add esp, 4\n");
    fprintf(f,"; Salto de línea\n");
    fprintf(f,"call print_endofline\n");
}

void gc_llamadas_fuciones (FILE* f,int valor, char * funcion){
    
    fprintf(f, "call _%s\n",funcion);
    fprintf(f, "add esp, 4*%d\n",valor);
    fprintf(f, "push dword eax\n" );
}
void gc_funcion (FILE* f, int num_var_local, char * nombre_f){
    fprintf(f, "_%s:\n",nombre_f);
    fprintf(f, "push ebp\n");
    fprintf(f, "mov ebp, esp\n");
    fprintf(f, "sub esp, %d\n", 4*num_var_local);

}

void fin_funcion (FILE* f, int es_direccion){
    fprintf(f, "pop dword edx\n");
    if (es_direccion==1){
        fprintf(f, "mov eax, [eax]\n");
    }
    fprintf(f, "mov dword esp, ebp\n");
    fprintf(f, "pop dword ebp\n");
    fprintf(f, "ret\n");

}

void gc_condicional(FILE* f,int etiqueta, int es_direccion){
    fprintf(f, "pop dword eax\n");
    fprintf(f, "pop dword edx\n");
    if(es_direccion==1){
        fprintf(f, "mov eax, [eax]\n");
    }
    fprintf(f, "cmp eax,0\n");
    fprintf(f, "je near fin_si%d\n",etiqueta);
}

void gc_condicional_simple_fin(FILE* f, int etiqueta){
    fprintf(f, "fin_si%d:  \n", etiqueta);
}
void gc_condSentencia(FILE* f, int etiqueta){
    fprintf(f, "jmp near fin_sino%d: \n", etiqueta);
    fprintf(f, "fin_si%d:  \n", etiqueta);
}
void gc_else(FILE* f, int etiqueta){
    fprintf(f, "fin_sino%d:\n",etiqueta);

}

void gc_bucleExp(FILE* f,int es_direccion, int etiqueta){
     fprintf(f, "pop dword eax\n");
     if (es_direccion==1)
     {
         fprintf(f, "mov eax [eax]\n");
     }
     fprintf(f, "cmp eax,0\n");
     fprintf(f, "je near fin_while%d \n", etiqueta);
    
}
void gc_inicioBucle(FILE* f, int etiqueta){
    fprintf(f, "inicio_while%d: \n", etiqueta );
}

void gc_finBucle(FILE* f, int etiqueta){
    fprintf(f, "jmp near inicio_while%d\n",etiqueta);
    fprintf(f, "fin_while%d: \n", etiqueta );
}


/*LLamada a la función scan_int*/
void gc_inicioMain(FILE* f,char *nombre){
    fprintf(f, "push dword _%s\n",nombre);
    fprintf(f, "call scan_int\n");
    fprintf(f, "add esp, 4\n" );

}

/*generacion de codigo segun sea el tipo para llamar a la funcion scanf*/
void gc_scanf(FILE* f, int tipo, char *nombre){
    fprintf(f, "push dword _%s\n",nombre);
    if(tipo==INT){
        fprintf(f, "call scan_int\n");    
    }else{
        fprintf(f, "call scan_boolean\n"); 
    }
    
    fprintf(f, "add esp, 4\n" );
}