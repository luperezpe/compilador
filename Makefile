
CC = gcc 
CFLAGS = -Wall
EXE = pruebaSintactico

all : lex y $(EXE)

.PHONY : clean
clean :
	rm -f *.o lex.yy.c y.tab.c y.output y.tab.h core $(EXE) 

$(EXE) :  pruebaSintactico.c tabla_hash.c  y.tab.h y.tab.c lex.yy.c tabla_hash.c 
	$(CC) $(CFLAGS) -o pruebaSintactico tabla_hash.c y.tab.c lex.yy.c generadorCodigo.c pruebaSintactico.c 

y:
	bison -y -d -v alfa.y
lex:
	flex alfa.l


